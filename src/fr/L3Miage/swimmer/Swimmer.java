package fr.L3Miage.swimmer;

import fr.L3Miage.action.Action;
import fr.L3Miage.action.DressingAction;
import fr.L3Miage.action.ManageResourcesAction;
import fr.L3Miage.action.SwimmingAction;
import fr.L3Miage.action.UndressingAction;
import fr.L3Miage.exception.ActionFinishedException;
import fr.L3Miage.resource.Basket;
import fr.L3Miage.resource.Cubicle;
import fr.L3Miage.resource.ResourcefulUser;
import fr.L3Miage.resourcePool.BasketPool;
import fr.L3Miage.resourcePool.CubiclePool;
import fr.L3Miage.scheduler.SequentialScheduler;

/**
 * A swimmer
 *
 */
public class Swimmer extends SequentialScheduler {

	/**
	 * The name of the swimmer
	 */
	protected String name;

	public Swimmer(String name, BasketPool the_basket, CubiclePool the_cubicle, int time_to_get_undressed,
			int swim_time, int time_to_get_dressed) {
		super();
		this.name = name;

		ResourcefulUser<Basket> rfu_basket = new ResourcefulUser<>();
		ResourcefulUser<Cubicle> rfu_cubicle = new ResourcefulUser<>();

		Action takeBasket = new ManageResourcesAction<>(rfu_basket, the_basket, true, name);
		Action takeCubicle1 = new ManageResourcesAction<>(rfu_cubicle, the_cubicle, true, name);
		Action get_undressed = new UndressingAction(time_to_get_undressed);
		Action freeCubicle1 = new ManageResourcesAction<>(rfu_cubicle, the_cubicle, false, name);
		Action swim = new SwimmingAction(swim_time);
		Action takeCubicle2 = new ManageResourcesAction<>(rfu_cubicle, the_cubicle, true, name);
		Action get_dressed = new DressingAction(time_to_get_dressed);
		Action freeCubicle2 = new ManageResourcesAction<>(rfu_cubicle, the_cubicle, false, name);
		Action freeBasket = new ManageResourcesAction<>(rfu_basket, the_basket, false, name);

		addAction(takeBasket);
		addAction(takeCubicle1);
		addAction(get_undressed);
		addAction(freeCubicle1);
		addAction(swim);
		addAction(takeCubicle2);
		addAction(get_dressed);
		addAction(freeCubicle2);
		addAction(freeBasket);
	}

	@Override
	public void executeStep() throws ActionFinishedException {
		System.out.println(name + "'s turn");
		super.executeStep();
	}

}
