package fr.L3Miage.action;

import java.util.ArrayList;

import fr.L3Miage.exception.ActionFinishedException;

/**
 * Abstract class representing an action
 */
public abstract class Action {

	protected boolean isInitialized = false;

	protected final ArrayList<Action> actions = new ArrayList<Action>();

	/**
	 * check if the action is ready
	 */
	public abstract boolean isReady();

	/**
	 * check if the action is in progress
	 */
	public boolean isInProgress() {
		return !this.isReady() && !this.isFinished();
	}

	/**
	 * check if the action is finished
	 */
	public abstract boolean isFinished();

	/**
	 * do a step on the action
	 */
	public void doStep() throws ActionFinishedException {
		if (isFinished())
			throw new ActionFinishedException();
		else
			executeStep();
	}

	/**
	 * Execute a step
	 * 
	 * @throws Exception
	 */
	protected abstract void executeStep() throws ActionFinishedException;

}
