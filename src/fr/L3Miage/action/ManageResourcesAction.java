package fr.L3Miage.action;

import fr.L3Miage.exception.ActionFinishedException;
import fr.L3Miage.resource.Resource;
import fr.L3Miage.resource.ResourcefulUser;
import fr.L3Miage.resourcePool.ResourcePool;

public class ManageResourcesAction<R extends Resource> extends Action {

	protected ResourcefulUser<R> resourceFulUser;
	protected ResourcePool resourcePool;
	protected boolean take, isFinished = false, isReady = true;
	protected String name;

	public ManageResourcesAction(ResourcefulUser<R> resourcefulUser, ResourcePool resourcePool, boolean take,
			String name) {
		this.resourceFulUser = resourcefulUser;
		this.resourcePool = resourcePool;
		this.take = take;
		this.name = name;
	}

	@Override
	public boolean isReady() {
		return isReady;
	}

	@Override
	public boolean isFinished() {
		return isFinished;
	}

	@SuppressWarnings("unchecked")
	@Override
	protected void executeStep() throws ActionFinishedException {
		isReady = false;
		if (take) {
			try {
				System.out
						.print(this.name + "'s trying to take ressource from " + resourcePool.getDescription() + "...");
				resourceFulUser.setResource((R) resourcePool.provideResource());
				isFinished = true;
				System.out.println(" success");
			} catch (Exception e) {
				System.out.println(" failed");
			}
		} else {
			System.out.println(this.name + "'s freeing resource from " + resourcePool.getDescription());
			resourcePool.freeResource(resourceFulUser.getResource());
			resourceFulUser.resetResource();
			isFinished = true;
		}
	}

}
