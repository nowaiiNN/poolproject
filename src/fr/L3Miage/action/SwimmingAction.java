package fr.L3Miage.action;

/**
 * The action of swimming
 */
public class SwimmingAction extends ForeseebleAction {

	public SwimmingAction(int totalTime) {
		super(totalTime);
	}

	@Override
	public void executeStep() {
		super.executeStep();
		System.out.println("swimming (" + getSpentTime() + "/" + totalTime + ")");
	}

}
