package fr.L3Miage.action;

/**
 * The action of dressing
 */
public class DressingAction extends ForeseebleAction {

	public DressingAction(int totalTime) {
		super(totalTime);
	}

	@Override
	public void executeStep() {
		super.executeStep();
		System.out.println("dressing (" + getSpentTime() + "/" + totalTime + ")");
	}

}
