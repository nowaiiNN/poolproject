/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.L3Miage.action;

/**
 * 
 * @author saab
 */
public class ForeseebleAction extends Action {

	protected final int totalTime; // Foreseeble action
	protected int remainingTime; // Forseeble action

	public ForeseebleAction(int totalTime) {
		super();

		this.totalTime = this.remainingTime = totalTime;
	}

	@Override
	public void executeStep() {
		this.remainingTime--;
	}

	@Override
	public boolean isFinished() {
		return this.remainingTime == 0;
	}

	@Override
	public boolean isReady() {
		return this.remainingTime == this.totalTime;
	}

	public int getSpentTime() {
		return 0 + totalTime - remainingTime;
	}

}