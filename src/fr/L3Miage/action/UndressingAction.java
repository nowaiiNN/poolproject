package fr.L3Miage.action;

/**
 * The action of Undressing
 */
public class UndressingAction extends ForeseebleAction {

	public UndressingAction(int totalTime) {
		super(totalTime);
	}

	@Override
	public void executeStep() {
		super.executeStep();
		System.out.println("undressing (" + getSpentTime() + "/" + totalTime + ")");
	}

}
