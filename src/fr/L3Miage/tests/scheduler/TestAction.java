package fr.L3Miage.tests.scheduler;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import fr.L3Miage.action.ForeseebleAction;
import fr.L3Miage.exception.ActionFinishedException;

public class TestAction {

	@Test
	public void testActionInitialized() {
		ForeseebleAction fa = new ForeseebleAction(10);

		assertTrue(fa.isReady());
		assertFalse(fa.isInProgress());
		assertFalse(fa.isFinished());
	}

	@Test
	public void testActionFinish() throws ActionFinishedException {
		int totalTime = 10;
		ForeseebleAction fa = new ForeseebleAction(totalTime);
		for (int i = 0; i < totalTime; i++) {
			fa.doStep();
		}
		assertFalse(fa.isReady());
		assertFalse(fa.isInProgress());
		assertTrue(fa.isFinished());
		assertEquals(10, fa.getSpentTime());
	}

	@Test
	public void testActionInProgress() throws ActionFinishedException {
		int Time = 5;
		ForeseebleAction fa = new ForeseebleAction(Time);
		fa.doStep();
		fa.doStep();
		assertEquals(2, fa.getSpentTime());
		assertFalse(fa.isReady());
		assertTrue(fa.isInProgress());
		assertFalse(fa.isFinished());
	}

}
