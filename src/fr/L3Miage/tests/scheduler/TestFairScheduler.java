package fr.L3Miage.tests.scheduler;

import static org.junit.Assert.assertTrue;

import java.util.NoSuchElementException;

import org.junit.Before;
import org.junit.Test;

import fr.L3Miage.action.Action;
import fr.L3Miage.action.ForeseebleAction;
import fr.L3Miage.action.OneStepAction;
import fr.L3Miage.exception.ActionFinishedException;
import fr.L3Miage.scheduler.FairScheduler;

public class TestFairScheduler {

	FairScheduler fs;

	// Initialize a new FairScheduler
	@Before
	public void init() {
		fs = new FairScheduler();
	}

	@Test(expected = NoSuchElementException.class)
	public void testDoStepOnEmptySchedulerThrowsException() throws ActionFinishedException {
		// We want to test if the NoSuchElementException is thrown by the
		// function, if it's thrown, it will be before the
		// ActionFinishedException
		fs.doStep();

	}

	@Test(expected = ActionFinishedException.class)
	public void testDoStepOnFinishedActionThrowsException() throws ActionFinishedException {
		// We want to test if the NoSuchElementException is thrown by the
		// function, if it's thrown, it will be before the
		// ActionFinishedException
		fs.addAction(new OneStepAction());
		fs.doStep();
		fs.doStep();

	}

	@Test
	public void actionsAreFairScheduled() throws ActionFinishedException {
		fs.addAction(new ForeseebleAction(2));
		fs.addAction(new ForeseebleAction(2));
		for (Action a : fs.getActions()) {
			assertTrue(a.isReady());
		}

		fs.doStep();
		fs.doStep();
		for (Action a : fs.getActions()) {
			assertTrue(a.isInProgress());
		}

		fs.doStep();
		fs.doStep();
		// The actions list has to be empty at this moment because they're all
		// finished
		assertTrue(fs.getActions().isEmpty());
	}

}
