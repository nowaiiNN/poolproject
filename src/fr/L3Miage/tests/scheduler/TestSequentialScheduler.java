package fr.L3Miage.tests.scheduler;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import fr.L3Miage.action.Action;
import fr.L3Miage.action.ForeseebleAction;
import fr.L3Miage.action.OneStepAction;
import fr.L3Miage.exception.ActionFinishedException;
import fr.L3Miage.scheduler.SequentialScheduler;

public class TestSequentialScheduler {

	SequentialScheduler fs;

	// Initialize a new FairScheduler
	@Before
	public void init() {
		fs = new SequentialScheduler();
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void testDoStepOnEmptySchedulerThrowsException() throws ActionFinishedException {
		// We want to test if the IndexOutOfBoundsException is thrown by the
		// ArrayList, if it's thrown, it will be before the
		// ActionFinishedException
		fs.doStep();

	}

	@Test(expected = ActionFinishedException.class)
	public void testDoStepOnFinishedActionThrowsException() throws ActionFinishedException {
		// We want to test if the NoSuchElementException is thrown by the
		// function, if it's thrown, it will be before the
		// ActionFinishedException
		fs.addAction(new OneStepAction());
		fs.doStep();
		fs.doStep();

	}

	@Test
	public void actionsAreSequentialScheduled() throws ActionFinishedException {
		fs.addAction(new ForeseebleAction(2));
		fs.addAction(new ForeseebleAction(2));
		for (Action a : fs.getActions()) {
			assertTrue(a.isReady());
		}

		fs.doStep();
		assertTrue(fs.getActions().get(0).isInProgress());
		assertTrue(fs.getActions().get(1).isReady());
		fs.doStep();
		assertTrue(fs.getActions().size() == 1);
		assertTrue(fs.getActions().get(0).isReady());

		fs.doStep();
		assertTrue(fs.getActions().get(0).isInProgress());
		fs.doStep();
		// The actions list has to be empty at this moment because they're all
		// finished
		assertTrue(fs.getActions().isEmpty());
	}

}
