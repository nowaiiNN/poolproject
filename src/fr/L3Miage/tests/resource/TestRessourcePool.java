package fr.L3Miage.tests.resource;

import static org.junit.Assert.assertEquals;

import java.util.NoSuchElementException;

import org.junit.Test;

import fr.L3Miage.resource.Resource;
import fr.L3Miage.resourcePool.CubiclePool;
import fr.L3Miage.resourcePool.ResourcePool;

public class TestRessourcePool {

	/**
	 * Test the initiation of Resource Pool
	 */
	@Test
	public void testInitResourcePool() {
		ResourcePool r = new CubiclePool(2);
		assertEquals(r.getResources().size(), 2);
		assertEquals(r.getUsedResources().size(), 0);
	}

	/**
	 * Test to provide a resource
	 */
	@Test
	public void testProvideRessource() {
		ResourcePool r = new CubiclePool(2);
		r.provideResource();
		r.provideResource();
		assertEquals(2, r.getUsedResources().size());
		assertEquals(r.getResources().size(), r.getUsedResources().size());
	}

	/**
	 * Test to provide more resource than the resourcePool contains
	 */
	@Test(expected = NoSuchElementException.class)
	public void testProvideRessourceException() {
		ResourcePool R = new CubiclePool(1);
		R.provideResource();
		R.provideResource();
	}

	/**
	 * Test to free a resource
	 */
	@Test
	public void testFreeRessource() {
		ResourcePool r = new CubiclePool(5);
		Resource resource = r.provideResource();
		assertEquals(r.getUsedResources().size(), 1);
		r.freeResource(resource);
		assertEquals(r.getUsedResources().size(), 0);
	}

	/**
	 * Test to free a resource that doesn't exist
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testFreeRessourceException() {
		ResourcePool r1 = new CubiclePool(3);
		r1.freeResource(null);
	}

}
