package fr.L3Miage.tests.resource;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import fr.L3Miage.resource.Cubicle;
import fr.L3Miage.resource.ResourcefulUser;

public class TestCubicle {
	Cubicle c;
	ResourcefulUser<Cubicle> rfu_cubicle1, rfu_cubicle2;

	@Before
	public void init() {
		c = new Cubicle();
		rfu_cubicle1 = new ResourcefulUser<Cubicle>();
	}

	@Test
	public void testResourceRoutine() {
		// We expect the basket to be stacked into the Resourceful User
		rfu_cubicle2 = rfu_cubicle1;
		rfu_cubicle1.setResource(c);
		assertEquals(c, rfu_cubicle1.getResource());
		rfu_cubicle1.resetResource();
		assertEquals(rfu_cubicle2, rfu_cubicle1);
		// We now have tested the 3 methods in ResourcefulUser
	}

	@Test
	public void getResourcesOnEmptyResourceIsNull() {
		rfu_cubicle1 = new ResourcefulUser<Cubicle>();
		assertEquals(rfu_cubicle1.getResource(), null);
	}

}
