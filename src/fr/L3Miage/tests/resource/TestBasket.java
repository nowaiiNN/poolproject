package fr.L3Miage.tests.resource;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import fr.L3Miage.resource.Basket;
import fr.L3Miage.resource.ResourcefulUser;

public class TestBasket {
	Basket b;
	ResourcefulUser<Basket> rfu_basket1, rfu_basket2;

	@Before
	public void init() {
		b = new Basket();
		rfu_basket1 = new ResourcefulUser<Basket>();
	}

	@Test
	public void testResourceRoutine() {
		// We expect the basket to be stacked into the Resourceful User
		rfu_basket2 = rfu_basket1;
		rfu_basket1.setResource(b);
		assertEquals(b, rfu_basket1.getResource());
		rfu_basket1.resetResource();
		assertEquals(rfu_basket2, rfu_basket1);
		// We now have tested the 3 methods in ResourcefulUser
	}

	@Test
	public void getResourcesOnEmptyResourceIsNull() {
		rfu_basket1 = new ResourcefulUser<Basket>();
		assertEquals(rfu_basket1.getResource(), null);
	}

}
