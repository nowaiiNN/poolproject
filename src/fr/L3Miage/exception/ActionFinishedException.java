package fr.L3Miage.exception;

/**
 * Exception throw when the actions are finished
 */
public class ActionFinishedException extends Exception {

	private static final long serialVersionUID = 1L;

	public ActionFinishedException() {
		super("No remaining actions");
	}

}
