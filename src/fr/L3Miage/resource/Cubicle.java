package fr.L3Miage.resource;

/**
 * Resource cubicle
 */
public class Cubicle implements Resource {

	/* (non-Javadoc)
	 * @see fr.L3Miage.resource.Resource#description()
	 */
	@Override
	public String description() {
		return "A cubicle";
	}

}
