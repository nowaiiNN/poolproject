package fr.L3Miage.resource;

/**
 * Resource Basket
 */
public class Basket implements Resource {

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.L3Miage.resource.Resource#description()
	 */
	@Override
	public String description() {
		return "A basket of clothes";
	}

}
