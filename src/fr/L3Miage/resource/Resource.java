package fr.L3Miage.resource;

/**
 * Interface representing a resource
 */
public interface Resource {
	/**
	 * @return the description of the resource
	 */
	public String description();
}
