package fr.L3Miage.resourcePool;

import fr.L3Miage.resource.Cubicle;

/**
 * Cubicle Pool
 */
public class CubiclePool extends ResourcePool {

	/**
	 * Create a number of cubicle
	 * 
	 * @param nbResource
	 *            the number of cubicle to create
	 */
	public CubiclePool(int nbResource) {
		for (int i = 0; i < nbResource; i++) {
			resources.add(new Cubicle());
		}
	}

	@Override
	public String getDescription() {
		return "pool cubicle";
	}

}
