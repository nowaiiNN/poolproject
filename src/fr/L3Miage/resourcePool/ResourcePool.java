package fr.L3Miage.resourcePool;

import java.util.ArrayList;
import java.util.NoSuchElementException;

import fr.L3Miage.resource.Resource;

/**
 * Resource Pool
 */
public abstract class ResourcePool {

	protected ArrayList<Resource> resources = new ArrayList<>();
	protected ArrayList<Resource> usedResources = new ArrayList<>();

	/**
	 * Get a pool resource
	 * 
	 * @return
	 * @throws NoSuchElementException
	 *             when there is no element in the pool
	 */
	public Resource provideResource() throws NoSuchElementException {
		if (resources.size() == usedResources.size()) {
			throw new NoSuchElementException();
		}

		for (Resource resource : resources) {
			if (!usedResources.contains(resource)) {
				usedResources.add(resource);
				return resource;
			}
		}

		return null;
	}

	/**
	 * Indicate that a pool resource has been released
	 * 
	 * @param resourceToFree
	 * @throws IllegalArgumentException
	 *             when the resourceToFree is not in the pool
	 */
	public void freeResource(Resource resourceToFree) throws IllegalArgumentException {
		if (!usedResources.contains(resourceToFree)) {
			throw new IllegalArgumentException();
		} else {
			usedResources.remove(resourceToFree);
		}
	}

	/**
	 * Get the description of the pool
	 * 
	 * @return
	 */
	public abstract String getDescription();

	/**
	 * @return the resources
	 */
	public ArrayList<Resource> getResources() {
		return resources;
	}

	/**
	 * @return the usedResources
	 */
	public ArrayList<Resource> getUsedResources() {
		return usedResources;
	}

}
