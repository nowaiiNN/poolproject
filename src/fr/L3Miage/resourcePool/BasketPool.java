package fr.L3Miage.resourcePool;

import fr.L3Miage.resource.Basket;

/**
 * Basket Pool
 */
public class BasketPool extends ResourcePool {

	/**
	 * Create a number of basket
	 * 
	 * @param nbResource
	 *            the number of basket to create
	 */
	public BasketPool(int nbResource) {
		for (int i = 0; i < nbResource; i++) {
			resources.add(new Basket());
		}
	}

	@Override
	public String getDescription() {
		return "pool basket";
	}

}
