package fr.L3Miage.scheduler;

import java.util.ArrayList;

import fr.L3Miage.action.Action;
import fr.L3Miage.exception.ActionFinishedException;

/**
 * A Scheduler action
 * 
 */
public abstract class Scheduler extends Action {

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.L3Miage.scheduler.Action#isReady()
	 */

	/**
	 * Returns the actions ArrayList, used for tests purpose
	 * 
	 * @return the actions from the Scheduler
	 */
	public ArrayList<Action> getActions() {
		return this.actions;
	}

	@Override
	public boolean isReady() {
		return isInitialized;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.L3Miage.scheduler.Action#isInProgress()
	 */
	@Override
	public boolean isInProgress() {
		return isInitialized && !isReady() && !isFinished();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.L3Miage.scheduler.Action#isFinished()
	 */
	@Override
	public boolean isFinished() {
		return isInitialized && actions.isEmpty();
	}

	public abstract void executeStep() throws ActionFinishedException;

	/**
	 * Add an action to the scheduler
	 * 
	 * @param subAction
	 */
	public void addAction(Action subAction) {
		if (subAction.isFinished()) {
			throw new IllegalArgumentException("Can’t add an already finished action");
		}
		if (isFinished()) {
			throw new IllegalStateException("You can’t add an action to a finished scheduler");
		} else {
			actions.add(subAction);
			isInitialized = true;
		}
	}

}
