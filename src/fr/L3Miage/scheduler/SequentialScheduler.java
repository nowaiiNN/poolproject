package fr.L3Miage.scheduler;

import fr.L3Miage.action.Action;
import fr.L3Miage.exception.ActionFinishedException;

/**
 * Sequential Scheduler
 */
public class SequentialScheduler extends Scheduler {

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.L3Miage.scheduler.Scheduler#executeStep()
	 */
	@Override
	public void executeStep() throws ActionFinishedException {
		Action nextAction = actions.get(0);
		nextAction.doStep();
		if (nextAction.isFinished()) {
			actions.remove(0);
		}
	}

}
