package fr.L3Miage.scheduler;

import java.util.Iterator;

import fr.L3Miage.action.Action;
import fr.L3Miage.exception.ActionFinishedException;

/**
 * Fair Scheduler
 * 
 */
public class FairScheduler extends Scheduler {

	private Iterator<Action> it;

	/**
	 * Execute a step on the next action
	 * 
	 * @throws Exception
	 */
	private void doStepOnAction() throws ActionFinishedException {
		Action currentAction;
		currentAction = it.next();
		currentAction.doStep();
		if (currentAction.isFinished()) {
			it.remove();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.L3Miage.scheduler.Scheduler#executeStep()
	 */
	public void executeStep() throws ActionFinishedException {
		if (it == null || !it.hasNext()) {
			it = actions.iterator();
		}
		doStepOnAction();

	}
}
